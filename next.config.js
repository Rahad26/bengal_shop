/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    FLEX_INTEGRATION_API_SECRET_KEY: '',
    FLEX_INTEGRATION_API_CLIENT_ID: '',
    FLEX_MARKETPLACE_API_CLIENT_ID: '097107b8-7d47-4e26-8b18-88cd9599357e',
    FLEX_MARKETPLACE_API_SECRET_KEY: '',
    GOOGLE_MAP_API_KEY: '',
    STRIPE_SECRET_KEY: '',
    STRIPE_PUBLISHABLE_KEY: '',
    NEXT_STRIPE_PUBLISHABLE_KEY: '',
  },
  images: {
    domains: ['i.ibb.co'],
  },
};
module.exports = nextConfig
