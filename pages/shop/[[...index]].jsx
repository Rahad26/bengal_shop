import { useEffect } from "react";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import Shop from "../../components/subPages/shop";
import { loadProductsData } from "../../store/components/ProductsSlice";

const ShopPage = ({ allProductsData }) => {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(loadProductsData(allProductsData))
    }, [dispatch, allProductsData])

    const router = useRouter()
    const subRoutes = router.query.index;
    return <div>
        <>
            <Shop subRoutes={subRoutes} />
        </>
    </div>;
};
export default ShopPage;

export async function getServerSideProps() {
    const response = await fetch(`https://bengal-shop-server.vercel.app/api/allproducts`)
    const data = await response.json()

    return {
        props: {
            allProductsData: data
        }
    }
}

