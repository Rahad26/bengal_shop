import Login from "../../components/subPages/login";
import LoginLayout from "../../layouts/loginLayout";

const LoginPage = () => {
    return <LoginLayout>
        <Login />
    </LoginLayout>;
};
export default LoginPage;
