import { Toaster } from 'react-hot-toast'
import { Provider } from 'react-redux'
import { store } from '../store'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return <Provider store={store}>
    <Component {...pageProps} />
    <Toaster />
  </Provider>
}
export default MyApp
