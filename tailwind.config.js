/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./layouts/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    screens: {
      'xxs': '300px',
      'xs': '411px',
      'sm': '540px',


      'smd': '640px',
      'md': '768px',

      'lmd': '912px',

      'lg': '1024px',
      'sxl': '1100px',
      'xl': '1280px',

      '2xl': '1400px',
      '3xl': '1920px',
    },
    extend: {
      colors: {
        dark_orange: '#FF5C00',
        dark_green: '#27AE60',
        custom_gray: '#333333',
        custom_gray2: '#4F4F4F',
        custom_gray3: '#828282',
        custom_gray4: '#666666',
        custom_skinColor: '#FFEBB7',
        custom_mild_sky: '#EBFAEB',
        off_white: '#F7F7F7',
        ash_white: '#BDBDBD',
        custom_golden: '#FABE50',
        custom_user_bg: '#F2F2F2',
      },
    },
  },
  plugins: [],
}
