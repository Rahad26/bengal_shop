import { AiFillCaretDown, AiOutlineHeart, AiOutlineShopping } from "react-icons/ai";
import { FaHospitalUser, FaSearch } from "react-icons/fa";
import { BiUser } from "react-icons/bi";
import { HiMenuAlt2 } from "react-icons/hi";
import { BsChevronDown } from "react-icons/bs";
import LogoWithName from "../../../components/reusable/LogoWithName";
import Link from "next/link";
import { useSelector, useDispatch } from "react-redux";
import toast from "react-hot-toast";
import { loadUserData } from "../../../store/components/currentUserSlice";
const sharetribeSdk = require('sharetribe-flex-sdk');


export default function Navbar() {
    const cartData = useSelector(state => state.CartSlice?.products);
    const dispatch = useDispatch()
    const cartCount = cartData.length;
    const clientId = process.env.FLEX_MARKETPLACE_API_CLIENT_ID;
    const sdk = sharetribeSdk.createInstance({
        clientId: clientId,
        baseUrl: 'https://flex-api.sharetribe.com/',
    });
    const currentUserId = useSelector(state => state?.currentUserSlice?.user?.id?.uuid);

    return <div className="mx-8 md:mx-12 lmd:mx-14 lg:mx-16 my-6 flex justify-center items-center">
        <div>
            <div className="grid xl:grid-cols-3 lmd:grid-cols-2 grid-cols-1">
                <LogoWithName />
                <div className="border-2 border-[#E0E0E0] rounded-full justify-around hidden sm:flex sm:w-[440px] lg:w-[500px] mt-4 lg:mt-0">
                    <span className="flex items-center lg:justify-center justify-end lg:text-center text-end p-3 text-custom_gray2 text-lg ml-7 pl-9 lg:pl-3 lg:ml-2"><FaSearch /></span>
                    <input className="py-2 px-10 border-none outline-none " type="text" placeholder='Search here' />
                    <button className="bg-custom_gray2 rounded-3xl py-2 px-6 lg:px-8 text-white my-[2px] cursor-pointer mr-9 lg:ml-9 lg:mr-0">Search</button>
                </div>
                <div className="flex xl:justify-end mt-3 ml-3 xl:mt-0 xl:ml-0 space-x-5 items-center">
                    <p className="cursor-pointer"><AiOutlineHeart /></p>
                    <Link href="/cart">
                        <a className="cursor-pointer relative">
                            <AiOutlineShopping className="text-lg" />
                            <span className="absolute top-[-20px] right-[-15px] h-4 w-4 flex justify-center items-center bg-[#FF5C00] rounded-full text-white text-xs">{cartCount}</span>
                        </a>
                    </Link>
                    <div className="h-[50px] w-[50px] rounded-full bg-custom_user_bg p-4 flex justify-center items-center cursor-pointer">
                        <p className=""><BiUser /></p>
                    </div>
                    {currentUserId ? <p onClick={() => {
                        sdk.logout().then(loginRes => {
                            console.log('My Log out', loginRes);
                            if (loginRes) {
                                toast.success('LogOut Success', { id: 'log-out' });
                                dispatch(loadUserData({}))
                            }
                        })
                    }} className="text-custom_gray2 cursor-pointer">Logout</p>
                        :
                        <Link href={'/login'} className="cursor-pointer">
                            <a className="text-custom_gray2 ml-1">
                                Login
                            </a>
                        </Link>}
                </div>
            </div>
            <div className="grid xl:grid-cols-3 lg:grid-cols-2 grid-cols-1 mt-8 lg:mt-5">
                <div className="flex justify-start items-center">
                    <div className="flex justify-evenly py-2 md:py-3 items-center font-semibold text-white bg-dark_green rounded-3xl cursor-pointer mb-5 lg:mb-0">
                        <p className="px-4 md:px-6 lg:px-8"><HiMenuAlt2 /></p>
                        <button className="px-4 md:px-6 lg:px-8">All Categories</button>
                        <p className="px-4 md:px-6 lg:px-8"><BsChevronDown /></p>
                    </div>
                </div>
                <div className="flex items-center">
                    <ul className="flex xl:space-x-8 xs:space-x-6 space-x-3 text-custom_gray2 text-sm">
                        <li className="cursor-pointer flex justify-between items-center space-x-2">
                            <Link href='/'>
                                <a>Home</a>
                            </Link>
                            <AiFillCaretDown />
                        </li>
                        <li className="cursor-pointer flex justify-between items-center space-x-2">
                            <Link href='/shop'>
                                <a>Shop</a>
                            </Link>
                            <AiFillCaretDown />
                        </li>
                        <li className="cursor-pointer flex justify-between items-center space-x-2">
                            <p>Pages</p>
                            <AiFillCaretDown />
                        </li>
                        <li className="cursor-pointer flex justify-between items-center space-x-2">
                            <p>Blogs</p>
                            <AiFillCaretDown />
                        </li>
                        <li className="cursor-pointer smd:flex items-center hidden">
                            <p>Contact</p>
                        </li>
                        <li className="cursor-pointer smd:flex hidden items-center whitespace-nowrap">
                            <p>Track Order</p>
                        </li>
                    </ul>
                </div>
                <div className="flex xl:justify-end items-center mt-5 lg:ml-3 xl:mt-0 xl:ml-0">
                    <p className="text-dark_orange">% Special Offers!</p>
                </div>
            </div>
        </div>
    </div >
}