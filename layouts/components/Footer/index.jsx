import LogoWithName from "../../../components/reusable/LogoWithName";
import LowerBottom from "./components/LowerBottom/index";

export default function Footer() {
    return <div className="bg-[#F5F5F5] 3xl:px-20 3xl:pt-20 3xl:pb-10 xl:px-16 xl:pt-16 xl:pb-8 md:px-10 md:pt-12 md:pb-6 px-6 pt-8 pb-5">
        <div className="grid lg:grid-cols-2 grid-cols-1 w-full">
            <div className="space-y-8">
                <LogoWithName />
                <p className="text-custom_gray2 text-sm max-w-lg">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla diam ornare nam est gravida. Netus viverra rhoncus sit magna sapien ac eget parturient id. Est luctus dapibus quam aliquam in nisl turpis. Elit et dictum lacus morbi nec accumsan a in.</p>
                <div className="flex h-12">
                    <img src='/images/apple_store.png' alt="" />
                    <img className="ml-2" src='/images/google_store.png' alt="" />
                </div>
            </div>
            <div className="grid grid-cols-2 lg:ml-40 lg:mt-4 mt-8">
                <div className="space-y-6">
                    <h1 className="text-xl font-semibold text-gray-600 ">About Us</h1>
                    <ul className="space-y-5 text-gray-500">
                        <li>About Karte</li>
                        <li>Contact</li>
                        <li>Career</li>
                        <li>Terms and Conditions</li>
                        <li>Category</li>
                    </ul>
                </div>
                <div className="space-y-6">
                    <h1 className="text-xl font-semibold text-gray-600 ">Info</h1>
                    <ul className="space-y-5 text-gray-500">
                        <li>Information</li>
                        <li>Shipping</li>
                        <li>Payment</li>
                        <li>Return</li>
                        <li>Blog</li>
                    </ul>
                </div>
            </div>
        </div>
        <LowerBottom />
    </div>
}