import { FaFacebookF, FaInstagram, FaSkype, FaTwitter } from 'react-icons/fa';

const LowerBottom = () => {
  return <div className='md:mt-24 mt-16 grid lg:grid-cols-3  grid-cols-1 gap-12'>
    <div className="grid grid-cols-4 w-64 mx-auto xl:mx-0 gap-3 order-1">
      <div className='bg-white h-14 w-14 justify-center rounded-md hover:bg-[#ff5e4d] text-custom_gray2 hover:text-white flex items-center'>
        <a href="#!"><FaFacebookF /></a>
      </div>
      <div className='bg-white h-14 w-14 justify-center rounded-md hover:bg-[#ff5e4d] text-custom_gray2 hover:text-white flex items-center'>
        <a href="#!"><FaInstagram /></a>
      </div>
      <div className='bg-white h-14 w-14 justify-center rounded-md hover:bg-[#ff5e4d] text-custom_gray2 hover:text-white flex items-center'>
        <a href="#!"><FaTwitter /></a>
      </div>
      <div className='bg-white h-14 w-14 justify-center rounded-md hover:bg-[#ff5e4d] text-custom_gray2 hover:text-white flex items-center'>
        <a href="#!"><FaSkype /></a>
      </div>
    </div>
    <div className='flex items-center order-3 lg:order-2'>
      <p className='text-sm text-[#828282] text-center mx-auto'>@2022 Copyright All Right Reserved by Bengal Shop</p>
    </div>
    <div className='flex justify-center items-center align-middle order-2 lg:order-3'>
      <img className='h-7 mx-auto' src="https://i.ibb.co/6ZhYzWg/payment.png" alt="" />
    </div>
  </div>;
};
export default LowerBottom;