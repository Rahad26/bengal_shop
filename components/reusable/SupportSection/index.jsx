const SupportSection = () => {
    return <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-6 my-20 mx-0 2xl:mx-5">
        <div className="flex">
            <div>
                <img src="/images/SupportBanner/call.png" alt="" />
            </div>
            <div className="flex items-center ml-5">
                <div className="space-y-2">
                    <h1 className="text-custom_gray font-semibold text-lg">24 Customer Support</h1>
                    <p className="text-[#828282] text-sm">Contact us 24 hours</p>
                </div>
            </div>
        </div>
        <div className="flex">
            <div>
                <img src="/images/SupportBanner/product.png" alt="" />
            </div>
            <div className="flex items-center ml-5">
                <div className="space-y-2">
                    <h1 className="text-custom_gray font-semibold text-lg">Authentic Products</h1>
                    <p className="text-[#828282] text-sm">Contact us 24 hours</p>
                </div>
            </div>
        </div>
        <div className="flex">
            <div>
                <img src="/images/SupportBanner/payment.png" alt="" />
            </div>
            <div className="flex items-center ml-5">
                <div className="space-y-2">
                    <h1 className="text-custom_gray font-semibold text-lg">Secure Payment</h1>
                    <p className="text-[#828282] text-sm">Contact us 24 hours</p>
                </div>
            </div>
        </div>
        <div className="flex">
            <div>
                <img src="/images/SupportBanner/offers.png" alt="" />
            </div>
            <div className="flex items-center ml-5">
                <div className="space-y-2">
                    <h1 className="text-custom_gray font-semibold text-lg">Best Prices & Offers</h1>
                    <p className="text-[#828282] text-sm">Contact us 24 hours</p>
                </div>
            </div>
        </div>
    </div>;
};
export default SupportSection;