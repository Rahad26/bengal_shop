import Link from "next/link";
import { useState } from "react";
import { FiChevronRight, FiChevronUp } from "react-icons/fi";
import { GiHamburgerMenu } from "react-icons/gi";
import MultiRangeSlider from "./components/RangeSlider";

const AllCategoriesBar = ({ setMax, setMin }) => {
    const [hamburger, isHamburger] = useState(false);
    const [freshShow, isFreshShow] = useState(false);
    const [greenShow, isGreenShow] = useState(false);

    return <div className="flex flex-col relative z-40">
        <div className="sticky top-10 lg:hidden" onClick={() => isHamburger(!hamburger)}>
            <p className="text-2xl"><GiHamburgerMenu /></p>
        </div>
        <div className={`${hamburger ? 'left-0' : 'left-[-600px]'} absolute lg:static w-60 smd:w-72 md:w-80 lg:w-full duration-500 ease-in-out top-10 bg-white rounded-xl z-40`} >
            <div className="border rounded-xl">
                <div className="flex items-center bg-custom_mild_sky py-4 px-24 rounded-t-xl">
                    <div className="flex items-center space-x-16">
                        <h1 className="text-custom_gray text-lg font-semibold">Categories</h1>
                        <div className="hidden lg:block">
                            <FiChevronUp />
                        </div>
                    </div>
                </div>
                <ul className="px-8 py-4 text-custom_gray text-sm space-y-6">
                    <li className="cursor-pointer flex items-center justify-between">
                        <Link href="/shop/vegetables">
                            <a>
                                <div className="flex items-center space-x-3">
                                    <img className="w-[30px} h-[30px]" src="https://i.ibb.co/1Q8Yncp/vegetables.png" alt="" />
                                    <p>Vegetables</p>
                                </div>
                            </a>
                        </Link>
                        <FiChevronRight />
                    </li>
                    <li onClick={() => {
                        isFreshShow(!freshShow)
                        isGreenShow(false)
                    }} className="cursor-pointer flex items-center justify-between">
                        <Link href="/shop/fruits">
                            <a>
                                <div className="flex items-center space-x-3">
                                    <img className="w-[30px} h-[30px]" src="https://i.ibb.co/pW8Sqfs/fruits.png" alt="" />
                                    <p>Fruits</p>
                                </div>
                            </a>
                        </Link>
                        <FiChevronRight />
                    </li>
                    <li onClick={() => isGreenShow(!greenShow)} className={`${freshShow ? 'flex items-center justify-between' : 'hidden'} cursor-pointer`}>
                        <Link href="/shop/fruits/fresh_fruits">
                            <a>
                                <p className="ml-10">Fresh Fruits</p>
                            </a>
                        </Link>
                        <FiChevronRight />
                    </li>
                    <li className={`${greenShow ? 'flex items-center justify-between' : 'hidden'} cursor-pointer`}>
                        <Link href="/shop/fruits/fresh_fruits/green_fruits">
                            <a>
                                <p className="ml-16 text-dark_green">Green Fruits</p>
                            </a>
                        </Link>
                        <FiChevronRight />
                    </li>
                    <li className="cursor-pointer flex items-center justify-between">
                        <Link href="/shop/groceries">
                            <a>
                                <div className="flex items-center space-x-3">
                                    <img className="w-[30px} h-[30px]" src="https://i.ibb.co/CMFxh3V/groceries.png" alt="" />
                                    <p>Groceries</p>
                                </div>
                            </a>
                        </Link>
                        <FiChevronRight />
                    </li>
                    <li className="cursor-pointer flex items-center justify-between">
                        <Link href="/shop/meat">
                            <a>
                                <div className="flex items-center space-x-3">
                                    <img className="w-[30px} h-[30px]" src="https://i.ibb.co/N7mBfWg/meat.png" alt="" />
                                    <p>Meat</p>
                                </div>
                            </a>
                        </Link>
                        <FiChevronRight />
                    </li>
                    <li className="cursor-pointer flex items-center justify-between">
                        <Link href="/shop/fish">
                            <a>
                                <div className="flex items-center space-x-3">
                                    <img className="w-[30px} h-[30px]" src="https://i.ibb.co/19zxdkY/fish.png" alt="" />
                                    <p>Fish</p>
                                </div>
                            </a>
                        </Link>
                        <FiChevronRight />
                    </li>
                    <li className="cursor-pointer flex items-center justify-between">
                        <Link href="/shop/beverage">
                            <a>
                                <div className="flex items-center space-x-3">
                                    <img className="w-[30px} h-[30px]" src="https://i.ibb.co/fY0drS4/beverage.png" alt="" />
                                    <p>Beverage</p>
                                </div>
                            </a>
                        </Link>
                        <FiChevronRight />
                    </li>
                    <li className="cursor-pointer flex items-center justify-between">
                        <Link href="/shop">
                            <a>
                                <div className="flex items-center space-x-3">
                                    <img className="w-[30px} h-[30px]" src="https://i.ibb.co/JxbKK5f/dry-food.png" alt="" />
                                    <p className="text-custom_gray text-sm">Dry food</p>
                                </div>
                            </a>
                        </Link>
                        <FiChevronRight />
                    </li>
                </ul>
            </div>
            <div className="border rounded-xl my-4">
                <div className="flex items-center bg-custom_mild_sky py-4 px-24 rounded-t-xl">
                    <div className="flex items-center space-x-16">
                        <h1 className="text-custom_gray text-lg font-semibold">Price</h1>
                        <div className="hidden lg:block">
                            <FiChevronUp />
                        </div>
                    </div>
                </div>
                <MultiRangeSlider
                    min={0}
                    max={1000}
                    onChange={({ min, max }) => {
                        setMax(max);
                        setMin(min);
                    }} />
            </div>
        </div>
    </div >;
};
export default AllCategoriesBar;