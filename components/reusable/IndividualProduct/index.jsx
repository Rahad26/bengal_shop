import { useState } from "react";
import { FaStar } from "react-icons/fa";
import { useDispatch } from "react-redux";
import { loadCartData } from "../../../store/components/CartSlice";
import ProductDetailsModal from "../DetailsModal";
import toast from 'react-hot-toast';

const IndividualProduct = ({ item, displayStyle }) => {
    const [cartedAmount, setCartedAmount] = useState(1);
    const dispatch = useDispatch();

    const [details, setDetails] = useState({});
    const [modalIsOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
        setDetails(item);
    }

    return <>
        <div className="flex justify-center items-center pb-0 my-5">
            <div className="flex justify-center">
                <div className={`text-center ${displayStyle === 'flex' ? 'flex space-x-5 items-center border px-20 py-4 rounded-xl shadow-lg' : 'space-y-1 lg:space-y-2 2xl:space-y-3'}`}>
                    <div>
                        <div className="relative group h-[240px] w-[200px] mx-auto">
                            <img className="rounded-xl" src={item?.img} alt="" />
                            <div className="inset-0 absolute top-0 opacity-0 group-hover:opacity-60 bg-black rounded-xl transition duration-200 ease-in-out flex items-center justify-center mx-auto h-[220px]">
                                <div className="flex">
                                    <p onClick={() => {
                                        if (cartedAmount >= 1) {
                                            setCartedAmount(cartedAmount - 1)
                                        }
                                    }} className="w-12 h-12 xl:w-12 xl:h-12 flex justify-center items-center   text-white text-xl  rounded-3xl border-spacing-4 border-2 cursor-pointer">-</p>
                                    <p className="mx-3 font-semibold w-12 h-12 flex justify-center items-center text-dark_green text-xl bg-white rounded-3xl border-spacing-4 border-2">{cartedAmount}</p>

                                    <p onClick={() => setCartedAmount(cartedAmount + 1)} className=" w-12 h-12 flex justify-center items-center text-white text-xl  rounded-3xl border-spacing-4 border-2 cursor-pointer">+</p>
                                </div>
                                {/* <div className="mt-5">
                                    <button onClick={openModal} className="text-sm text-white hover:border px-4 py-1 rounded-xl">Details</button>
                                </div> */}
                                <div>
                                    {/* <ProductDetailsModal
                                        modalIsOpen={modalIsOpen}
                                        setIsOpen={setIsOpen}
                                        item={item}
                                    ></ProductDetailsModal> */}
                                </div>
                            </div>

                        </div>

                    </div>
                    <div className="space-y-1">
                        <i className="text-custom_golden"><FaStar className="inline" /><FaStar className="inline" /><FaStar className="inline" /><FaStar className="inline" /><FaStar className="inline" /></i><span className="text-custom_gray2 text-sm ml-2">({item?.totalRatedNum})</span>
                        <h1 className="text-custom_gray font-semibold text-lg">{item?.name}</h1>
                        <p className=""><span className="text-custom_gray font-semibold text-lg">${item?.offerPrice}</span><span className="line-through text-ash_white text-sm ml-3">${item?.price}</span></p>
                        <button onClick={() => {
                            dispatch(loadCartData({ item, cartedAmount }))
                            toast.success('Added to Cart', { id: 'add-success' })
                        }} className="text-dark_green hover:text-white text-md py-2 lg:py-3 px-6 lg:px-9 hover:bg-dark_green rounded-3xl border">Add to Cart</button>
                    </div>
                </div>
            </div>
        </div>
    </>
};
export default IndividualProduct;
