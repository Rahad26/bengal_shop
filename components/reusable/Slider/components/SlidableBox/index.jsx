import { useState } from "react";
import { FaStar } from "react-icons/fa";
import { useDispatch } from "react-redux";
import { loadCartData } from "../../../../../store/components/CartSlice";
import toast from 'react-hot-toast';

const SlidableBox = ({ item, content }) => {
    const [cartedAmount, setCartedAmount] = useState(1);
    const dispatch = useDispatch()

    if (content === 'category') {
        return <div className="bg-off_white hover:bg-custom_mild_sky h-[180px] w-[220px] flex justify-center items-center rounded-sm">
            <div className="pb-10 mx-2">
                <div>
                    <img src={item.img} alt="" />
                </div>
                <div>
                    <p className="text-center">{item.name}</p>
                </div>
            </div>
        </div>
    }
    if (content === 'brands') {
        return <div className="flex justify-center items-center pb-5">
            <img src={item.img} alt="" />
        </div>
    }
    if (content === 'products') {
        const { name, img, rated, totalRatedNum, offerPrice, price, category, branch, subBranch, sku } = item;
        return <>
            <div className="flex justify-center items-center pb-0">
                <div className="flex justify-center">
                    <div className="text-center space-y-1 lg:space-y-2 2xl:space-y-3">
                        <div>
                            <div className="relative group h-[300px] w-[200px] mx-auto">
                                <img className="rounded-xl" src={img} alt="" />
                                <div className="inset-0 absolute top-0 opacity-0 group-hover:opacity-60 bg-black rounded-xl transition duration-200 ease-in-out flex items-center justify-center mx-auto h-[220px]">
                                    <div className="flex">
                                        <p onClick={() => {
                                            if (cartedAmount >= 1) {
                                                setCartedAmount(cartedAmount - 1)
                                            }
                                        }} className="w-12 h-12 xl:w-12 xl:h-12 flex justify-center items-center   text-white text-xl  rounded-3xl border-spacing-4 border-2">-</p>
                                        <p className="mx-3 font-semibold w-12 h-12 flex justify-center items-center text-dark_green text-xl bg-white rounded-3xl border-spacing-4 border-2">{cartedAmount}</p>

                                        <p onClick={() => setCartedAmount(cartedAmount + 1)} className=" w-12 h-12 flex justify-center items-center text-white text-xl  rounded-3xl border-spacing-4 border-2">+</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <i className="text-custom_golden"><FaStar className="inline" /><FaStar className="inline" /><FaStar className="inline" /><FaStar className="inline" /><FaStar className="inline" /></i><span className="text-custom_gray2 text-sm ml-2">({totalRatedNum})</span>
                        <h1 className="text-custom_gray font-semibold text-lg">{name}</h1>
                        <p className=""><span className="text-custom_gray font-semibold text-lg">${offerPrice}</span><span className="line-through text-ash_white text-sm ml-3">${price}</span></p>
                        <button onClick={() => {
                            dispatch(loadCartData({ item, cartedAmount }))
                            toast.success('Added to cart', { id: 'add-success-now' })
                        }} className="text-dark_green hover:text-white text-md py-2 lg:py-3 px-6 lg:px-9 hover:bg-dark_green rounded-3xl border">Add to Cart</button>
                    </div>
                </div>
            </div>
        </>
    }
};
export default SlidableBox;