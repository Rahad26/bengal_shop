import React, { useRef, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";


import { Autoplay, Pagination, Navigation } from "swiper";
import SlidableBox from "./components/SlidableBox";

export default function SliderComponent({ items, content }) {
    let height = '250px';
    if (content === 'brands') {
        height = '140px';
    }
    if (content === 'products') {
        height = '500px';
    }
    const style = {
        "height": height,
    }
    return (
        <div className="my-5">
            <div className="">
                <Swiper
                    style={style}
                    slidesPerView={4}
                    centeredSlides={true}
                    spaceBetween={10}
                    grabCursor={false}
                    pagination={{
                        type: "progressbar",
                    }}
                    breakpoints={{
                        50: {
                            slidesPerView: 1,
                            spaceBetween: 20,
                        },
                        411: {
                            slidesPerView: 1,
                            spaceBetween: 20,
                        },
                        540: {
                            slidesPerView: 2,
                            spaceBetween: 30,
                        },
                        680: {
                            slidesPerView: 3,
                            spaceBetween: 30,
                        },
                        1024: {
                            slidesPerView: 4,
                            spaceBetween: 40,
                        },
                        1280: {
                            slidesPerView: 5,
                            spaceBetween: 40,
                        },
                        1490: {
                            slidesPerView: 6,
                            spaceBetween: 40,
                        },
                        1700: {
                            slidesPerView: 7,
                            spaceBetween: 40,
                        },
                        1920: {
                            slidesPerView: 8,
                            spaceBetween: 50,
                        },
                    }}
                    navigation={true}
                    modules={[Autoplay, Pagination, Navigation]}
                    className="mySwiper "
                    autoplay={{
                        delay: 3000,
                        disableOnInteraction: false,
                    }}
                >
                    <div className="">
                        {
                            items.map((item, index) => <SwiperSlide key={index} className="flex justify-center items-center flex-col  pt-10 cursor-pointer">
                                <SlidableBox key={item?.id} item={item} content={content}></SlidableBox>
                            </SwiperSlide>)
                        }
                    </div>
                </Swiper>
            </div>
        </div >
    );
}
