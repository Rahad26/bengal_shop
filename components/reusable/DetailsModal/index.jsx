import Modal from 'react-modal';
import Image from 'next/image';
import { AiFillStar } from 'react-icons/ai';

const customStyles = {
    content: {
        top: '5%',
        left: '45%',
        right: 'auto',
        bottom: '0',
        zIndex: '50 !important',
        backgroundColor: 'white',
        height: '70vh',
        width: '50%',
    },
};
const ProductDetailsModal = ({ setIsOpen, modalIsOpen, item }) => {
    const {
        img,
        name,
        offerPrice,
        price,
    } = item;

    function closeModal() {
        setIsOpen(false);
    }
    return (
        <div>
            <Modal
                isOpen={modalIsOpen}
                setIsOpen={setIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel='Example Modal'
                ariaHideApp={false}
            >
                <div className='grid grid-cols-12 z-50'>
                    <div className='col-span-5 basis-[100px] xs:w-2/4 md:w-full'>
                        <Image
                            width={200}
                            height={200}
                            // layout='fill'
                            src={img}
                            alt='image not found'
                            className='object-cover'
                        ></Image>
                    </div>
                    <div className='col-span-7 space-y-5 md:mr-10 xs:w-2/4 md:w-full'>
                        <div className='flex items-center space-x-8'>
                            <p className='uppercase text-sm text-gray-400'>status</p>
                            <p className='uppercase text-xs md:text-sm text-green-400 font-semibold'>
                                In Stock
                            </p>
                        </div>
                        <div className='name'>
                            <h1 className='text-xs md:text-xl font-semibold'>{name}</h1>
                        </div>
                        <div className='flex space-x-5'>
                            <div className='text-yellow-400 flex'>
                                <i>
                                    <AiFillStar />
                                </i>
                                <i>
                                    <AiFillStar />
                                </i>
                                <i>
                                    <AiFillStar />
                                </i>
                                <i>
                                    <AiFillStar />
                                </i>
                                <i>
                                    <AiFillStar />
                                </i>
                            </div>
                            <div className=''>
                                <p className='text-sm'>10 review</p>
                            </div>
                        </div>
                        <div className='flex space-x-5 items-center'>
                            <div className=''>
                                <p className='font-semibold text-xl'>${offerPrice}</p>
                            </div>
                            <div className=''>
                                <p className='line-through text-gray-400'>${price}</p>
                            </div>
                            <div className='hidden md:block'>
                                <p className='text-gray-400 text-sm'>(+15% Vat Included)</p>
                            </div>
                        </div>
                        <div className='hidden md:block'>
                            <p className='text-gray-600 text-sm'>
                                12 Products Sold in last 11 hours
                            </p>
                        </div>
                        <div className=''>
                            <hr />
                        </div>
                    </div>
                </div>
            </Modal>
        </div>
    );
};

export default ProductDetailsModal;