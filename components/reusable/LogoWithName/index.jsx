const LogoWithName = () => {
    return <div className="flex">
        <div>
            <img className="h-[58px] w-[65px]" src="/images/logo.png" alt="" />
        </div>
        <div className="flex items-center ml-2">
            <h1 className="text-custom_gray font-semibold text-2xl">Bengal shop</h1>
        </div>
    </div>;
};
export default LogoWithName;