const brandData = [
    {
        img: 'https://i.ibb.co/KDV3rBF/b3.png',
        id: 1,
    },
    {
        img: 'https://i.ibb.co/VLsXHbr/b4.png',
        id: 2,
    },
    {
        img: 'https://i.ibb.co/X7VtzNG/b5.png',
        id: 3,
    },
    {
        img: 'https://i.ibb.co/XVpWhRw/b6.png',
        id: 4,
    },
    {
        img: 'https://i.ibb.co/0MfHnXV/b1.png',
        id: 5,
    },
    {
        img: 'https://i.ibb.co/GvB0PqD/b2.png',
        id: 6,
    },
]

export default brandData;