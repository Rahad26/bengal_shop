import HomeLayout from "../../../layouts/homeLayout/index";
import SupportSection from "../../reusable/SupportSection";
import CarouselBanner from "./components/CarouselBanner";
import CategoryBanner from "./components/CategoryBanner";
import DealsOfTheWeek from "./components/DealsOfTheWeek";
import FruitOfferBanner from "./components/FruitOfferBanner";
import PopularBrands from "./components/PopularBrands";
import SearchByCategory from "./components/SearchByCategory";


export default function Home() {
    return <>
        <HomeLayout>
            <div className="mx-2 md:mx-4 lmd:mx-5 lg:mx-10">
                <CarouselBanner />
                <SearchByCategory />
                <FruitOfferBanner />
                <DealsOfTheWeek />
                <PopularBrands />
                <CategoryBanner />
                <SupportSection />
            </div>
        </HomeLayout>
    </>
}