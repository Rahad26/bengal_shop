import SliderComponent from "../../../../reusable/Slider";
import brandData from "../../utils/brandData";

const PopularBrands = () => {
    const items = brandData;
    return <div className="my-20">
        <h1 className="text-custom_gray2 font-semibold text-2xl">Search by Category</h1>
        <SliderComponent items={items} content="brands" />
    </div>;
};
export default PopularBrands;