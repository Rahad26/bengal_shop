import SliderComponent from "../../../../reusable/Slider";
import category from "../../utils/categoryData";

const SearchByCategory = () => {
    const items = category;
    return <div className="xl:my-20 lg:my-14 md:my-12 my-10">
        <h1 className="text-custom_gray2 font-semibold text-2xl">Search by Category</h1>
        <SliderComponent items={items} content="category" />
    </div>;
};
export default SearchByCategory;