import SliderComponent from "../../../../reusable/Slider";
import productCollection from "../../utils/productData";

const DealsOfTheWeek = () => {
    const items = productCollection;
    return <div className="my-20">
        <h1 className="text-custom_gray2 font-semibold text-2xl">Deals of the Week</h1>
        <div className="">
            <SliderComponent items={items} content="products" />
        </div>
    </div>;
};
export default DealsOfTheWeek;