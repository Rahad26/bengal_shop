import { AiOutlineArrowRight } from 'react-icons/ai';

const CategoryBanner = () => {
    return <div className='grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-4 my-20'>
        <div className='flex items-center bg-[#FFF0DC] px-8 py-8 lg:py-0 rounded-xl justify-between'>
            <div>
                <h1 className='text-custom_gray font-bold text-3xl'>Fresh Fruits Collection</h1>
                <div>
                    <div className='flex justify-center items-center w-12 h-12 bg-white rounded-full text-[#FF5C00] hover:bg-[#FF5C00] hover:text-white mt-5 md:mt-14 cursor-pointer'>
                        <AiOutlineArrowRight />
                    </div>
                </div>
            </div>
            <div>
                <img src="images/CategoryBanner/fruits.png" alt="" />
            </div>
        </div>
        <div className='flex items-center bg-[#DDF1D6] px-8 py-8 lg:py-0 rounded-xl justify-between'>
            <div>
                <h1 className='text-custom_gray font-bold text-3xl'>Vegetable Collection</h1>
                <div>
                    <div className='flex justify-center items-center w-12 h-12 bg-white rounded-full text-[#FF5C00] hover:bg-[#FF5C00] hover:text-white mt-5 md:mt-14 cursor-pointer'>
                        <AiOutlineArrowRight />
                    </div>
                </div>
            </div>
            <div>
                <img src="images/CategoryBanner/vegetable.png" alt="" />
            </div>
        </div>
        <div className='flex items-center bg-custom_skinColor px-8 py-8 lg:py-0 rounded-xl justify-between'>
            <div>
                <h1 className='text-custom_gray font-bold text-3xl'>Grocery Item</h1>
                <div>
                    <div className='flex justify-center items-center w-12 h-12 bg-white rounded-full text-[#FF5C00] hover:bg-[#FF5C00] hover:text-white mt-5 md:mt-14 cursor-pointer'>
                        <AiOutlineArrowRight />
                    </div>
                </div>
            </div>
            <div>
                <img src="images/CategoryBanner/grocery.png" alt="" />
            </div>
        </div>
    </div >;
};
export default CategoryBanner;