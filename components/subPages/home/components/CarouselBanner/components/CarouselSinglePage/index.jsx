/* eslint-disable @next/next/no-img-element */
import Image from "next/image";

const CarouselSinglePage = ({ bgColor = '' }) => {
    return <div className={`${bgColor === 'skin' ? 'bg-custom_skinColor' : 'bg-custom_mild_sky'}  
    lg:py-4 md:py-2 py-1 grid grid-cols-2`}>
        <div className="flex items-center lg:mx-16 mx-1.5 md:mx-5">
            <div className="xl:space-y-5 md:space-y-3 space-y-2">
                <p className="text-dark_green font-semibold text-xs lg:text-sm">Save up 30% off</p>
                <h1 className="font-bold xl:text-3xl md:text-xl text-sm text-custom_gray">Bengal Vegetable farm Organic 100%</h1>
                <p className="text-custom_gray2 xl:text-lg md:text-sm text-[10px] mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vehicula faucibus massa est elit maecenas.</p>
                <div>
                    <button className="text-white xl:text-lg md:text-sm text-xs lg:py-3 md:py-2 py-1 lg:px-10 md:px-6 px-4 bg-dark_green rounded-3xl hover:text-orange-400 lg:mt-4 md:mt-2 mt-1">Shop Now</button>
                </div>
            </div>
        </div>
        <div className="w-full flex justify-center">

            <div>
                <img className="h-[35vh] md:h-[50vh] lg:h-[70vh] xl:h-[75vh]" src="/images/CarouselBanner/carousel-1.png" alt="" />
            </div>

        </div>
    </div>
};
export default CarouselSinglePage;
