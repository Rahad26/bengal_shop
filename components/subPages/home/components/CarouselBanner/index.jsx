import React, { useRef, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";


import "swiper/css/navigation";

import { Pagination, Navigation } from "swiper";
import CarouselSinglePage from "./components/CarouselSinglePage";

export default function CarouselBanner() {
    return (
        <div className="">
            <Swiper
                className=" h-[40vh] md:h-[55vh] lg:h-[75vh] xl:h-[80vh]"
                direction={"vertical"}
                pagination={{
                    clickable: true,
                }}
                modules={[Pagination, Navigation]}
            >
                <SwiperSlide>
                    <CarouselSinglePage />
                </SwiperSlide>
                <SwiperSlide>
                    <CarouselSinglePage bgColor={'skin'} />
                </SwiperSlide>
                <SwiperSlide>
                    <CarouselSinglePage />
                </SwiperSlide>
            </Swiper>
        </div >
    );
}
