const TopBanner = () => {
    return <div className="relative" >
        <img className="min-h-[90px]" src="/images/ShopTopBanner/top_banner.png" alt="" />
        <div className="absolute top-1 sm:top-3 smd:top-5 md:top-10 sxl:top-12 2xl:top-20 left-1 sm:left-3 smd:left-5 md:left-10 sxl:left-12 2xl:left-20 space-y-2">
            <h3 className="text-dark_orange text-[10px] sm:text-xs smd:text-sm md:text-lg 2xl:text-xl">Buy 1 Get 1</h3>
            <h1 className="text-dark_green font-bold text-sm smd:text-lg md:text-xl 2xl:text-3xl w-2/4 xs:w-3/4">Up to 30% Discount on Selected Items</h1>
        </div>
    </div >;
};
export default TopBanner;