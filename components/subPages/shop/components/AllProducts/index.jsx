import IndividualProduct from "../../../../reusable/IndividualProduct";

const AllProducts = ({ displayStyle, filteredData }) => {
    return <div>
        <div className={`${displayStyle === 'grid' ? 'grid md:grid-cols-3 sm:grid-cols-2 lg:grid-cols-2 xl:grid-cols-3 grid-cols-1' : 'flex flex-col justify-center items-center'} my-10`}>
            {filteredData &&
                filteredData.map(item => <IndividualProduct key={item?.id} displayStyle={displayStyle} item={item} />)
            }
        </div>
        {
            filteredData.length === 0 && <h1 className="text-custom_gray text-xl my-10 text-center font-semibold">No Data Found</h1>
        }
    </div>;
};
export default AllProducts;