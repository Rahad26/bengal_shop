import { useEffect, useState } from "react";
import { BiMenuAltRight } from "react-icons/bi";
import { CgMenuGridR } from "react-icons/cg";
import { FiChevronDown } from "react-icons/fi";
import ShopLayout from "../../../layouts/shopLayout";
import AllCategoriesBar from "../../reusable/AllCategoriesBar";
import SupportSection from "../../reusable/SupportSection";
import AllProducts from "./components/AllProducts";
import TopBanner from "./components/TopBanner";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { loadFilteredData } from "../../../store/components/FilteredSlice";

export default function Shop({ subRoutes }) {
    const allProductsData = useSelector(state => state.productsSlice?.products);
    const [displayStyle, setDisplayStyle] = useState('grid');
    const [filteredData, setFilteredData] = useState(allProductsData);
    const [max, setMax] = useState(1000);
    const [min, setMin] = useState(0);
    function filterData(subRoutes) {
        let targetData;
        if (!subRoutes) {
            setFilteredData(allProductsData);

            let priceFiltered = allProductsData.filter(product => product.offerPrice >= min && product.offerPrice <= max);
            setFilteredData(priceFiltered);
        }
        if (subRoutes?.length === 1) {
            targetData = allProductsData.filter(product => product.category.toLowerCase().includes(subRoutes[0]));
            setFilteredData(targetData);

            let priceFiltered = targetData.filter(product => product.offerPrice >= min && product.offerPrice <= max);
            setFilteredData(priceFiltered);

        }
        if (subRoutes?.length === 2) {
            targetData = allProductsData.filter(product => product.subBranch.toLowerCase().includes(subRoutes[1].split('_')[0]));
            setFilteredData(targetData);

            let priceFiltered = targetData.filter(product => product.offerPrice >= min && product.offerPrice <= max);
            setFilteredData(priceFiltered);
        }
        else if (subRoutes?.length === 3) {
            targetData = allProductsData.filter(product => product.branch.toLowerCase().includes(subRoutes[2].split('_')[0]));
            setFilteredData(targetData);

            let priceFiltered = targetData.filter(product => product.offerPrice >= min && product.offerPrice <= max);
            setFilteredData(priceFiltered);
        }
    }
    useEffect(() => {
        filterData(subRoutes);
    }, [allProductsData, max, min])

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(loadFilteredData(filteredData))
    }, [dispatch, filteredData])

    return <>
        <ShopLayout>
            <div className="mx-10">
                <div className="flex space-x-4">
                    <div><AllCategoriesBar setMax={setMax} setMin={setMin} /></div>
                    <div>
                        <TopBanner />
                        <h1 className="text-custom_gray font-bold text-2xl text-left my-5">Fruits Collection</h1>
                        <div className="flex justify-between">
                            <div className="text-[15px] flex flex-col md:flex-row space-y-1 md:space-y-0 md:space-x-3">
                                <p className="text-custom_gray3">Fruits<span className="md:mx-3 mx-1">|</span></p>
                                <p className="text-custom_gray3">Green Fruits<span className="md:mx-3 mx-1">|</span></p>
                                <p className="text-custom_gray font-semibold">Fresh Fruits</p>
                            </div>
                            <div className="flex flex-col md:flex-row md:space-x-4 space-y-4 md:space-y-0">
                                <h1 className="text-custom_gray text-sm text-start"><span className="font-semibold">{filteredData.length}</span> Products Found</h1>
                                <div className="flex space-x-2">
                                    <BiMenuAltRight onClick={() => setDisplayStyle('flex')} className="text-custom_gray2 text-xl cursor-pointer" />
                                    <CgMenuGridR onClick={() => setDisplayStyle('grid')} className="text-ash_white text-xl cursor-pointer" />
                                    <div className="cursor-pointer xs:flex justify-between items-center space-x-2 text-custom_gray4 hidden ">
                                        <p>Default Sorting</p>
                                        <FiChevronDown />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <AllProducts displayStyle={displayStyle} filteredData={filteredData} subRoutes={subRoutes} />
                    </div>
                </div>
            </div>
            <div className="mx-10">
                <SupportSection />
            </div>
        </ShopLayout>
    </>
}