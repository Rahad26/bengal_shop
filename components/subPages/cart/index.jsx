import { useSelector } from "react-redux";
import ShopLayout from "../../../layouts/shopLayout";
import DataTable from "./components/DataTable";

const CartComponent = () => {
    const cartData = useSelector(state => state.CartSlice?.products);
    let grandTotal = 0;
    let grandQty = 0;
    cartData.forEach(element => {
        grandTotal += (Number(element.quantity) * Number(element.product.offerPrice));
        grandQty += (Number(element.quantity))
    });

    return <>
        <ShopLayout>
            <h1 className="text-center text-xl text-custom_gray my-8">Welcome to Cart</h1>
            <table className="border-2 xs:mx-auto my-8 mx-2">
                <thead>
                    <tr >
                        <th className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-1">Image</th>
                        <th className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-1">Item</th>
                        <th className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-1">Quantity</th>
                        <th className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-1">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        cartData.map(item => <DataTable key={item?.sku} item={item} />)
                    }
                    <tr className="border p-5 ">
                        <td className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-2">Grand Total</td>
                        <td className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-2">=</td>
                        <td className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-2">{grandQty}</td>
                        <td className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-2">{grandTotal}</td>
                    </tr>
                </tbody>
            </table>
        </ShopLayout>
    </>
};
export default CartComponent;