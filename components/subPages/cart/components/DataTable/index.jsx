const DataTable = ({ item }) => {
    return <tr className="border p-5 ">
        <td className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-2"><img className="w-20 h-20 rounded-full mx-2" src={item.product.img} alt="" /></td>
        <td className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-2">{item.product.name}</td>
        <td className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-2">{item.quantity}</td>
        <td className="px-2 text-center sm:px-5 smd:px-8 lg:px-20 py-2">{item.product.offerPrice * item.quantity}</td>
    </tr>;
};
export default DataTable;