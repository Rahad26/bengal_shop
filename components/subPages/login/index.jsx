import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import toast from 'react-hot-toast';
import { useDispatch } from 'react-redux';
import { loadUserData } from '../../../store/components/currentUserSlice';
const sharetribeSdk = require('sharetribe-flex-sdk');

const Login = () => {

    const [firstName, setFirstName] = useState('');
    const [lastname, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [myCurrentUser, setMyCurrentUser] = useState(null);
    const router = useRouter();
    const dispatch = useDispatch();
    const clientId = process.env.FLEX_MARKETPLACE_API_CLIENT_ID;
    const sdk = sharetribeSdk.createInstance({
        clientId: clientId,
        baseUrl: 'https://flex-api.sharetribe.com/',
    });
    // const sdk = sharetribeSdk.createInstance({ clientId });
    const handleLoginClick = () => {
        console.log(email, password, firstName, lastname);

        sdk.login({
            username: email,
            password: password,
        }).then(loginRes => {
            console.log("Login successful.");
            // console.log(loginRes);
            if (loginRes.status === 200) {
                console.log(loginRes);
                sdk.currentUser?.show().then(res => {
                    console.log(res?.data?.data);
                    setMyCurrentUser(res?.data?.data);
                    dispatch(loadUserData(res?.data?.data));
                    router.push('/');
                });
            }
        });
    }

    // useEffect(() => {
    //     if (myCurrentUser) {
    //         sdk.currentUser?.show().then(res => {
    //             // console.log(res);
    //             if (res) {
    //                 setMyCurrentUser(res?.data?.data);
    //             }
    //             else {
    //                 console.log('No User');
    //             }
    //         });
    //     }
    //     // console.log(sdk.currentUser);
    //     // console.log(currentUser);
    // }, [myCurrentUser?.id?.uuid])

    const handleSignUpClick = () => {
        sdk.currentUser.create({
            email,
            password,
            firstName: firstName,
            lastName: lastname,
            displayName: `${firstName}`,
            bio: `Hello, I am. ${lastname}`,
            publicData: {
                age: 27
            },
            protectedData: {
                phoneNumber: "+1-202-555-5555"
            },
            privateData: {
                discoveredServiceVia: "Twitter"
            }
        }, {
            expand: true
        }).then(res => {
            console.log(res);
            if (res.status === 200) {
                handleLoginClick();
            }
        });
    }

    return (
        <div>
            <section className="mb-20">
                <div className="px-6 h-full text-gray-800">
                    <div
                        className="flex xl:justify-center lg:justify-between justify-center items-center flex-wrap h-full g-6">
                        <div className="grow-0 shrink-1 md:shrink-0 basis-auto xl:w-6/12 lg:w-6/12 md:w-9/12 mb-12 md:mb-0">
                            <img src="https://i.ibb.co/HgZhzV1/login-logo.png" className="w-full" alt="login-logo" />
                        </div>
                        <div className="xl:ml-20 xl:w-5/12 lg:w-5/12 md:w-8/12 mb-12 md:mb-0">
                            <form>
                                <div className="flex flex-row items-center justify-center lg:justify-start">
                                    <p className="text-lg mb-0 mr-4">Sign in with</p>
                                    <button

                                        type="button"
                                        data-mdb-ripple="true"
                                        data-mdb-ripple-color="light"
                                        className="inline-block p-3 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded-full shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out mx-1"
                                    >
                                        {/* -- Google login -- */}
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 488 512" className="w-4 h-4">
                                            <path
                                                fill="currentColor"
                                                d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z" /></svg>
                                    </button>
                                </div>

                                <div
                                    className="flex items-center my-4 before:flex-1 before:border-t before:border-gray-300 before:mt-0.5 after:flex-1 after:border-t after:border-gray-300 after:mt-0.5"
                                >
                                    <p className="text-center font-semibold mx-4 mb-0">Or</p>
                                </div>
                                {/* -- First Name input -- */}
                                <div className="mb-6">
                                    <input
                                        onBlur={(e) => setFirstName(e.target.value)}
                                        type="text"
                                        name="firstName"

                                        className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        id="exampleFormControlInput1"
                                        placeholder="Your Fist Name"
                                    />
                                </div>
                                {/* -- Last Name input -- */}
                                <div className="mb-6">
                                    <input
                                        onBlur={(e) => setLastName(e.target.value)}
                                        type="text"
                                        name="lastName"

                                        className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        id="exampleFormControlInput2"
                                        placeholder="Your Last Name"
                                    />
                                </div>

                                {/* -- Email input -- */}
                                <div className="mb-6">
                                    <input
                                        onBlur={(e) => setEmail(e.target.value)}
                                        type="text"
                                        name="email"
                                        required
                                        className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        id="exampleFormControlInput3"
                                        placeholder="Email address"
                                    />
                                </div>
                                {/* -- Password input -- */}
                                <div className="mb-6">
                                    <input
                                        onBlur={(e) => setPassword(e.target.value)}
                                        name="password"
                                        required
                                        type="password"
                                        className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        id="exampleFormControlInput4"
                                        placeholder="Password"
                                    />
                                </div>

                                <div className="text-center lg:text-left">
                                    <button
                                        onClick={() => {
                                            if (email || password) {
                                                if (!email.includes('@')) {
                                                    toast.error('Invalid E-mail ID', { id: 'email@Error' })
                                                }
                                                else if (/^.{2,}$/.test(password)) {
                                                    handleLoginClick()
                                                } else if (!/^.{2,}$/.test(password)) {
                                                    toast.error('Password Must be at-least 8 characters', { id: 'passError' })
                                                }
                                            } else {
                                                toast.error('Email & password required', { id: 'inputError' })
                                            }
                                        }}
                                        type="button"
                                        className="inline-block px-7 py-3 bg-blue-600 text-white font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">
                                        Login </button>
                                </div>

                                <div className="text-center lg:text-left">
                                    <button
                                        onClick={() => {
                                            if (email || password) {
                                                if (!email.includes('@')) {
                                                    toast.error('Invalid E-mail ID', { id: 'email@Error' })
                                                }
                                                else if (/^.{2,}$/.test(password)) {
                                                    handleSignUpClick()
                                                } else if (!/^.{2,}$/.test(password)) {
                                                    toast.error('Password Must be at-least 8 characters', { id: 'passError' })
                                                }
                                            } else {
                                                toast.error('Email & password required', { id: 'inputError' })
                                            }
                                        }}
                                        type="button"
                                        className="inline-block px-7 py-3 bg-blue-600 text-white font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">
                                        Sign Up </button>
                                </div>
                                <div className="text-center lg:text-left">
                                    <button
                                        onClick={() => {
                                            sdk.logout().then(loginRes => {
                                                // console.log("Logout successful.");
                                                console.log('My Log out', loginRes);
                                                toast.success('LogOut Success', { id: 'log-out' })
                                            });
                                        }}
                                        type="button"
                                        className="inline-block px-7 py-3 bg-red-600 text-white font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">
                                        LogOut </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default Login;