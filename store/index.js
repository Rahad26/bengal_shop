import { configureStore } from '@reduxjs/toolkit'
import productsSlice from './components/ProductsSlice/index'
import FilteredSlice from './components/FilteredSlice/index'
import CartSlice from './components/CartSlice/index'
import currentUserSlice from './components/currentUserSlice/index.jsx'

export const store = configureStore({
    reducer: {
        productsSlice,
        FilteredSlice,
        CartSlice,
        currentUserSlice,
    },
})