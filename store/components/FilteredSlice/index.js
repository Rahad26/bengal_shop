import { createSlice } from '@reduxjs/toolkit'

const initialState = { products: [] }

export const FilteredSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        loadFilteredData: (state, action) => {
            state.products = action.payload
        },
    },
})

export const { loadFilteredData } = FilteredSlice.actions
export default FilteredSlice.reducer