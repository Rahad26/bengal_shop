import { createSlice } from '@reduxjs/toolkit'

const initialState = { products: [] }

export const CartSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        loadCartData: (state, action) => {
            const cartData = {
                product: action.payload.item,
                quantity: action.payload.cartedAmount
            }
            state.products = [...state.products, cartData]
        },
    },
})

export const { loadCartData } = CartSlice.actions
export default CartSlice.reducer